//
//  Current.swift
//  Stormy
//
//  Created by Mo Lotfi on 13/03/15.
//  Copyright (c) 2015 Mo Lotfi. All rights reserved.
//

import Foundation
import UIKit



struct Current {
    var currentTime: Int
    var temprature: Int
    var humadity : Double
    var precipProbability: Double //chance of rain
    var summary: String
    var icon: String
    
    init(weatherDictionary: NSDictionary) {
        let currentWeather = weatherDictionary["currently"] as NSDictionary
        
        currentTime = currentWeather["time"] as Int
        temprature = currentWeather["temperature"] as Int
        humadity = currentWeather["humidity"] as Double
        precipProbability = currentWeather["precipProbability"] as Double
        summary = currentWeather["summary"] as String
        icon = currentWeather["icon"] as String
        
    
    }
    
    func dateFromUnix
    
}
