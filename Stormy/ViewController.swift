//
//  ViewController.swift
//  Stormy
//
//  Created by Mo Lotfi on 10/03/15.
//  Copyright (c) 2015 Mo Lotfi. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    private let apiKey = "a6522b656538ad25d9123033264823b1"
    
    

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        
        let baseURL = NSURL(string: "https://api.forecast.io/forecast/\(apiKey)/")
        
        let forecastURL = NSURL(string: "53.549297,10.060665/", relativeToURL:baseURL!)
        
        let sharedSession = NSURLSession.sharedSession()
        
        let downloadTask : NSURLSessionDownloadTask = sharedSession.downloadTaskWithURL(forecastURL!, completionHandler: { (location :NSURL!, response: NSURLResponse!, error: NSError!) -> Void in
            
            if (error == nil) {
                let dataObject = NSData(contentsOfURL: location)
                let weatherDictionary: NSDictionary  = NSJSONSerialization.JSONObjectWithData(dataObject!, options: nil, error: nil) as NSDictionary
                //println(weatherDictionary)
            
                //grap Current Dic from WeatherDictioanry
                //let currentWeatherDictionary: NSDictionary = weatherDictionary["currently"] as NSDictionary
                //println(currentWeatherDictionary)
                
                
                //createing a new instance of Current struct by passing the weather dictionary to get initialised 
                let currentWeather = Current(weatherDictionary: weatherDictionary)
                
                println(currentWeather.temprature)
                
                
                
            }
            
            
            //This is to check if we have the JSON data
            //var urlContents = NSString(contentsOfURL: Location, encoding: NSUTF8StringEncoding, error: nil) 
            //println(urlContents)
            
        
           
        })
        
        downloadTask.resume()
        
        
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()

    
    }


}

